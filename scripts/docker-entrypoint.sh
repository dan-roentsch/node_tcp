#!/bin/sh

cd /home/notify
export NEW_RELIC_APP_NAME="Notify_$DEPLOY_ENV"
pm2 start
pm2 logs --raw
