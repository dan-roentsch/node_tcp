#!/bin/sh

echo '=======BUILD======='
cd ..
chmod 777 gradlew
./gradlew buildProd
echo '=======TEST======='
if [ "$1" = "--test" ] || [ "$1" = "-t" ]; then
    ./gradlew testBuild
fi
echo '=======IMAGE======='
cd ./dist/config
version=$(cat config.json | grep version | awk '{print $2}' | sed 's/\"//' | sed 's/\",//')
echo "$version"
cd ../..
docker build --no-cache -t notify .
docker tag notify containers.mheducation.com/notify:"$version"
echo '=======TAG IN GITHUB========'
git tag "$version"
echo '=======PUSH TO REGISTRY========'
docker push containers.mheducation.com/notify:"$version"
