#!/bin/sh

echo '=======BUILD========='
cd ..
./gradlew buildProd
echo '=======IMAGE======='
cd ./dist/config
version=`cat config.json | grep version | awk '{print $2}' | sed 's/\"//' | sed 's/\",//'`
echo "$version"
cd ../..
docker build --no-cache -t notify .
docker tag notify containers.mheducation.com/notify:"$version"
echo '=======PUSH TAG TO GITHUB========'
git tag "$version"
git push origin "$version"
echo '=======PUSH TO REGISTRY========'
docker push containers.mheducation.com/notify:"$version"
