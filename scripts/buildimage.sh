#!/bin/sh



echo '=======CLEANUP======='
cd ..
docker stop ntfy
docker rm ntfy
docker rmi -f containers.mheducation.com/notify
docker rmi -f notify
echo '=======BUILD========='
./gradlew buildProd
echo '=======IMAGE======='
cd ./dist/config
version=`cat config.json | grep version | awk '{print $2}' | sed 's/\"//' | sed 's/\",//'`
echo "$version"
cd ../..
docker build --no-cache -t notify .
docker tag notify containers.mheducation.com/notify:"$version"
