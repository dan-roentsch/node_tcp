#!/bin/sh

pm2 set pm2-logrotate:max_size 10K
pm2 set pm2-logrotate:compress true
pm2 set pm2-logrotate:rotateInterval '*/10 * * * *'
cd ../dist
pm2 start
pm2 logs --raw NOTIFY
