#!/bin/sh

echo '========Build SERVER ==================='
gulp prod
echo '========Start Server!========================='
cd ..
# This next line actually won't work.  The env variables are gone with the child shell
# as soon as node loads.
# BUT when testing outside the container on local, you can
# cut and paste this line into a bash console
KAFKA_BROKER_HOST=10.221.27.106:9002,10.221.16.188:9002,10.221.11.116:9002 ZOOKEEPER_HOST=10.221.9.158:9001,10.221.24.77:9001,10.221.18.127:9001 node dist/app.js | node_modules/.bin/bunyan
