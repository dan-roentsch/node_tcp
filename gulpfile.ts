import del from 'del';
import * as gulp from 'gulp';
import mocha from 'gulp-mocha';
import replace from 'gulp-replace';
import tslint from 'gulp-tslint';
import ts from 'gulp-typescript';
import { NodeLib } from './nodeLib';

let tsProject = ts.createProject('tsconfig.json');
let lib = new NodeLib();
const DEST = './dist';
const LINT_CONFIG = 'tslint.json';
const SOURCE = './src';
const SOURCE_TS = `${SOURCE}/**/*.ts`;
const SOURCE_JS = `${SOURCE}/**/*.js`;
const SOURCE_TEST = `${DEST}/**/*.spec.js`;
const NODE_DIR = 'node_modules';
const CONFIG_DEST = './dist/config/';
const CONFIG_SRC = './config/**/*';

async function copyDependencies() {
    let libs: any[] = lib.libs();
    await libs.forEach((mod) => {
        gulp.src(mod.src)
            .pipe(gulp.dest(`${DEST}/${mod.dest}`));
    });
    gulp.src([`${SOURCE}/ecosystem.config.js`, `${SOURCE}/newrelic.js`])
        .pipe(gulp.dest(DEST));
}
async function updateVersion(isProd: boolean) {
    let d = new Date();
    let b: string;
    if (isProd) {
        b = "B_"
    } else {
        b = "";
    }
    await gulp.src([CONFIG_SRC])
        .pipe(replace(/LOCAL/, `NOTIFY_${b}${d.getFullYear()}${d.getMonth() + 1}${d.getDate()}_${d.getHours()}${d.getMinutes()}`))
        .pipe(gulp.dest(CONFIG_DEST));
}
gulp.task('lint', () => {
    return gulp.src(SOURCE_TS)
        .pipe(tslint({
            configuration: LINT_CONFIG,
            formatter: 'prose',
        }))
        .pipe(tslint.report({
            allowWarnings: false,
            emitError: false,
        }));
});

gulp.task('cleanup', () => {
    return del(DEST)
        .then(() => {
            console.info(`Deleted ${DEST}`);
        });
});

gulp.task('cleanup.test', () => {
    return del(SOURCE_TEST)
        .then(() => {
            console.info(`Deleted ${SOURCE_TEST}`);
        });
});
gulp.task('build', () => {
    let result = gulp.src([SOURCE_TS, `!${SOURCE}/**/testing/`, `!${SOURCE}/**/testing/**/*`, `!${SOURCE}/**/*.test.ts`])
        .pipe(tsProject());
    return result.js.pipe(gulp.dest(DEST));
});

gulp.task('buildjs', () => {
    return gulp.src([SOURCE_JS, SOURCE_TS, `!${SOURCE}/**/test/`, `!${SOURCE}/**/test/**/*`, `!${SOURCE}/**/*.test.ts`])
        .pipe(gulp.dest(DEST));
});

gulp.task('build.test', () => {
    let result = gulp.src([`${SOURCE}/**/test/`, `${SOURCE}/**/test/**/*.ts`, `${SOURCE}/testing/tests/**/*.spec.ts`])
        .pipe(tsProject());
    return result.js.pipe(gulp.dest(DEST));
});
gulp.task('test.built.js', () => {
    return gulp.src(`${DEST}/test/*.js`, { read: false })
        .pipe(mocha());
});
gulp.task('update.version', () => {
    return updateVersion(false);
});
gulp.task('update.branch.version', () => {
    return updateVersion(true);
});
gulp.task('copy.dependencies', () => {
    return copyDependencies();
});

gulp.task('prod', gulp.series(['cleanup', 'update.version', 'lint', 'build', 'copy.dependencies']));
gulp.task('branch', gulp.series(['cleanup', 'update.branch.version', 'lint', 'build', 'copy.dependencies']));
gulp.task('prodjs', gulp.series(['cleanup', 'update.version', 'buildjs', 'copy.dependencies']));
gulp.task('test', gulp.series(['build.test', 'test.built.js']));
