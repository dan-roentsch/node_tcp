import chai from 'chai';
import chaiHttp from 'chai-http';
import mocha from 'mocha';
import { Application } from '../app';
import { Routing } from '../modules/lib/Routing.Class';
import { ILogger } from '../modules/model/ILogger';
import { MockLog } from './model/mocks';

const should = chai.should();
chai.use(chaiHttp);
let main: Application;
let l: ILogger;

before(() => {
    main = new Application();
    l = new MockLog();
});
after(() => {
    main.server.close();
});
describe('The application...', () => {
    it('Should read the config file', (done) => {
        main.boot(l).then(() => {
            main.ConfigParams.should.be.a('object');
            main.ConfigParams.should.have.property('version');
            main.ConfigParams.should.have.property('log');
            main.ConfigParams.should.have.property('serverPort');
            main.ConfigParams.should.have.property('topics');
            main.ConfigParams.should.have.property('monitorTimeout');
            done();
        });
    });
    it('Should return the version', (done) => {
        main.boot(l).then((logger) => {
            let routing = new Routing(main.ConfigParams, main.app, logger);
            main.setRouting(routing);
            main.setServer();
            chai
                .request(main.server)
                .get('/version')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
    it('Should create an array of 2 circuits', (done) => {
        main.boot(l).then((logger) => {
            let routing = new Routing(main.ConfigParams, main.app, logger);
            main.setRouting(routing);
            main.buildModules();
            main.circuits.should.have.property('length');
            main.circuits.length.should.equal(2);
            main.circuits.forEach((circuit) => circuit.destroyCircuit());
            done();
        });
    });
});
