import { ITopic } from './ITopic';

export interface IConfig {
    version?: string;
    ignoreTopics?: string[];
    zookeeperHost?: string;
    kafkaBrokerHost?: string;
    newRelicAppName?: string;
    serverPort?: string;
    consumerGroups?: string[];
    log?: { folder: string; stream: string; level: string };
    topics?: ITopic[];
    monitorTimeout: number;
}
