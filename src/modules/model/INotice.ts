import { Sender } from './Sender.enum';

export interface INotice<T> {
    sentBy: Sender;
    message: T;
}
