export interface IHealthcheckMessage {
    type: string;
    content: number;
    topic: string;
}
