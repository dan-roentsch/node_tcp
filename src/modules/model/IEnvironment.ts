export interface IEnvironment {
    name: string;
    topics: string[];
}
