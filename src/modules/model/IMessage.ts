import { IResource } from './IResource';

export interface IMessage {
    eventRaisedDate?: string;
    eventGUID?: string;
    eventSourceHost?: string;
    room?: string;
    type?: string;
    content?: any;
}
