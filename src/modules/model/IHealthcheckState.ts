export interface IHealthcheckState {
    status: number;
    kafkaTopic: string;
}
