export interface IResource {
    providerResourceId: string;
    authorId: string;
    title: string;
    weight: number;
    resourceType: string;
    scoreCalculationType: string;
}
