import { IMessage } from './IMessage';

export interface IEvent {
    topic?: string;
    offset?: number;
    partition?: number;
    key?: number;
    value?: string;
}
