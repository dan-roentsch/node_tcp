import * as os from 'os';
import Q from 'q';
import SocketIOClient from 'socket.io-client';
import { IConfig } from '../model/IConfig';
import { IHealthcheckMessage } from '../model/IHealthcheckMessage';
import { IHealthcheckState } from '../model/IHealthcheckState';
import { ILogger } from '../model/ILogger';
import { ITopic } from '../model/ITopic';
import { KafkaProducer } from './KafkaProducer.Class';
const ROOM = 'health-room';

export class HealthCheck {
    private kafkaProducer: KafkaProducer;
    private HEALTHCHECK: string;
    private topics: ITopic[];
    private logger: ILogger;
    private accumulator: string[];
    private config: IConfig;
    private timeoutIds: any;

    constructor(configuration: IConfig,
                theLogger: ILogger,
                producer: KafkaProducer) {
        this.logger = theLogger;
        this.config = configuration;
        this.topics = this.config.topics;
        this.kafkaProducer = producer;
        this.HEALTHCHECK = 'healthcheck-' + os.hostname();
        this.timeoutIds = {};
    }
    public pulse(onChecksComplete: (status: number, message?: string) => void) {

        this.accumulator = [];

        let promises = this.topics.map(this.sendReceiveMessage);
        Q.allSettled(promises)
            .then((results) => {
                results.map((result) => this.accumulateErrors(result));
            })
            .then(() => {
                if (this.accumulator && this.accumulator.length) {
                    // convert to a js object literal for easy conversion to JSON
                    let failedTopics = {
                        fail: this.accumulator,
                    };
                    onChecksComplete(500, JSON.stringify(failedTopics));
                } else {
                    onChecksComplete(200);
                }
            })
            .catch((reason) => {
                this.logger.info({ 'error reason': reason });
                onChecksComplete(500, reason);
            });
    }
    private sendReceiveMessage = (topic: ITopic) => {
        let deferred = Q.defer();
        Q.fcall(() => {
            this.heardHealthCheckMessage(topic, (err: boolean, theTopic: string) => {
                let state = {
                    kafkaTopic: theTopic,
                    status: 200,
                };
                if (err) {
                    state.status = 500;
                    state.kafkaTopic = theTopic;
                }
                return deferred.resolve(state);
            });
        });
        return deferred.promise;
    }
    private heardHealthCheckMessage = (topic: ITopic,
                                       onResult: (failed: boolean, topicName: string) => void) => {
        // if we timeout before hearing anything, return fail.
        this.timeoutIds[topic.nsp] = setTimeout(() => {
                                                onResult(true, topic.name);
                                                }, this.config.monitorTimeout);

        this.listenForMessage(topic, onResult,
                                () => this.sendKafkaPayload(topic,
                                                            this.buildMessage(topic.name)));
    }
    private accumulateErrors(result: any) {
        if (result.value && result.value.status && result.value.status === 500) {
            this.accumulator.push(result.value.kafkaTopic);
        }
    }
    private sendKafkaPayload(topic: ITopic, message: string) {
        this.kafkaProducer.sendPayload(topic.name, message);
    }
    private listenForMessage(topic: ITopic,
                             onResult: (failed: boolean, topicName: string) => void,
                             onConnectHandler: () => void) {
        let listener = SocketIOClient(`http://localhost:${this.config.serverPort}`, {
                                                path: `/${topic.nsp}`, transports: ['websocket'],
                                    });

        listener.on('error', (err: string) => {
            this.logger.error({ error: err }, 'Error listening on healthcheck client socket');
        });

        listener.on('connect', () => {
            this.logger.info(`Healthcheck socket client connected for topic ${topic.name}`);
            onConnectHandler();
        });

        listener.on(this.HEALTHCHECK, () => {
            if (this.timeoutIds[topic.nsp]) {
                clearTimeout(this.timeoutIds[topic.nsp]);
            }
            onResult(false, topic.name);
        });
    }
    private buildMessage(topicName: string): string {
        let gotTime = new Date().getTime();
        let message: IHealthcheckMessage = {
            content: gotTime,
            topic: topicName,
            type: this.HEALTHCHECK,
        };

        return JSON.stringify(message);
    }
}
