import { ConsumerGroup, ConsumerGroupOptions, ZKOptions } from 'kafka-node';
import * as os from 'os';
import { IConfig } from '../model/IConfig';
import { IEvent } from '../model/IEvent';
import { ILogger } from '../model/ILogger';
import { IMessage } from '../model/IMessage';

export class KafkaConsumer {
  private restarting: boolean;
  private consumer: ConsumerGroup;
  private eventHandler: (notice: IMessage) => void;
  private try: number;
  private currentTimeout: any;

  constructor(
    private logger: ILogger,
    private configParams: IConfig,
    private topics: string[],
  ) {
    this.buildConsumerGroup();
  }

  public setEventHandler(handler: (event: IMessage) => void) {
    this.eventHandler = handler;
  }

  public closeConsumer(): void {
    this.consumer.close(true, (error) => {
      this.logger.error(`Error ${error} closing consumer.`);
    });
  }
  private errorHandler = (err: string) => {
    this.logger.error(err, {
      message: 'KAFKAERROR happened (consumer)',
      try: this.try,
    });
    if (this.try > 2) {
      this.manageConnectRetryTimeout(false); // false = clear existing timer, but don't start a new one
      this.restart();
    } else {
      this.manageConnectRetryTimeout(true);  // true = clear old timer, start new one.
    }
  }

  private handleKafkaMessage = (ev: IEvent) => {
    try {
      if (this.eventHandler) {
        let msg: IMessage = JSON.parse(ev.value);
        this.logger.info({ Message: msg, topic: ev.topic }, 'Event received');
        this.eventHandler(msg);
      }
    } catch (e) {
      this.logger.error(e, 'Error happened in handler');
    }
  }

  private restart() {
    if (this.restarting) {
      return;
    }
    this.restarting = true;
    this.logger.info('Restarting Kafka consumer');
    if (this.consumer != null) {
      this.consumer.close(true, () => {
        this.logger.info('Kafka consumer stopped for rebuild in 30 seconds.');
        // true means force = true.
        setTimeout(() => {
          this.buildConsumerGroup();
        }, 1000 * 30);
      });
    }
  }

  private buildConsumerGroup() {
    const uniqueGroupId = `Notify-${os.hostname()}`;
    const zkoptions: ZKOptions = {
      retries: 2,
      sessionTimeout: 15000,
      spinDelay: 10000,
    };
    const options: ConsumerGroupOptions = {
      groupId: uniqueGroupId,
      host: this.configParams.zookeeperHost,
      id: uniqueGroupId,
      protocol: ['roundrobin'],
      sessionTimeout: 15000,
      zk: zkoptions,
    };

    this.consumer = new ConsumerGroup(options, this.topics);

    this.consumer.on('message', this.handleKafkaMessage);
    this.consumer.on('error', this.errorHandler);

    this.restarting = false;
    this.try = 0;
    this.logger.info(`Kafka consumer group ${uniqueGroupId} started`);
  }
  private manageConnectRetryTimeout(reset: boolean): void {
    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout);
    }
    // if no retries in 30 seconds (3 * spin delay) assume reconnection and reset tries to 0
    if (reset === true) {
      this.try++;
      this.currentTimeout = setTimeout(() => {
        this.try = 0;
      }, 30000);
    }
  }
}
