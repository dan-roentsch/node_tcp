import { Server } from 'http';
import SocketIO from 'socket.io';
import { IConfig } from '../model/IConfig';
import { ILogger } from '../model/ILogger';
import { IMessage } from '../model/IMessage';
import { KafkaConsumer } from './KafkaConsumer.Class';
import { KafkaProducer } from './KafkaProducer.Class';

export class Circuit {
    private io: SocketIO.Server;
    private server: Server;
    private isNarrowCast: boolean;
    private socketName: string;
    private topicName: string;
    private logger: ILogger;
    private nsp: SocketIO.Namespace;
    private config: IConfig;
    private kafkaConsumer: KafkaConsumer;

    constructor(socketName: string, topicName: string,
                server: Server, logger: ILogger,
                config: IConfig,
                isNarrowCast: boolean) {
        this.socketName = socketName;
        this.topicName = topicName;
        this.server = server;
        this.isNarrowCast = isNarrowCast;
        this.logger = logger;
        this.config = config;
        this.kafkaConsumer = new KafkaConsumer(this.logger, this.config, [this.topicName]);
        this.createCircuit();
    }
    public destroyCircuit() {
        this.io.close();
        this.kafkaConsumer.closeConsumer();
    }
    private bindSocketEvents() {
        this.logger.info(`Binding socket events`);
        this.io = SocketIO(this.server, { path: this.socketName });
        if (!this.isNarrowCast) {
            this.io.on('connection', (socket) => {
                this.logger.info(`user connected to socket server for broadcast`);
                this.nsp = socket.nsp;
            });
            this.io.on('error', () => {
                this.logger.error('Error connecting to socket for broadcast.');
            });
        } else {
            let room: any;
            let sox: SocketIO.Socket;
            this.io.on('connection', (socket) => {
                sox = socket;
                room = sox.handshake.query.room;
                sox.join(room);
                this.logger.info({ room, socketId: sox.id }, 'New connection for narrowcast');
                this.nsp = sox.nsp;
            });
            this.io.on('disconnect', (reason: string) => {
                this.logger.info({ room, socketId: sox.id, reason }, 'Dropped connection');
            });
            this.io.on('error', () => {
                this.logger.error('Error connecting to socket for narrowcast.');
            });
        }
    }
    private onEvent = (event: IMessage) => {
        if (this.isNarrowCast) {
            this.io.to(event.room).emit(event.type, event.content);
            this.logger.info(`Pushed event to room ${event.room}`);
        } else {
            this.io.emit(event.type, event.content);
            this.logger.info(`Pushed event to ${this.socketName}`);
        }
    }
    private createCircuit() {
        this.kafkaConsumer.setEventHandler(this.onEvent);
        this.bindSocketEvents();
    }
}
