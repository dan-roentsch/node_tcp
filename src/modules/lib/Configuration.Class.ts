import * as fs from 'fs';
import { join } from 'path';
import * as Util from 'util';
import { IConfig } from '../model/IConfig';

const CONFIG_FILE = join(__dirname, '../../config/config.json');
export class Configuration {
    constructor() {
    }
    public async getParams(): Promise<IConfig> {
        const readFile = Util.promisify(fs.readFile);
        const file = await readFile(CONFIG_FILE, 'utf-8');
        let parsedConfig: IConfig = JSON.parse(file);
        parsedConfig.kafkaBrokerHost = process.env.KAFKA_BROKER_HOST;
        parsedConfig.zookeeperHost = process.env.ZOOKEEPER_HOST;
        parsedConfig.newRelicAppName = process.env.NEW_RELIC_APP_NAME;
        return parsedConfig;
    }
}
