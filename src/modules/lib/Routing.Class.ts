import * as bodyParser from 'body-parser';
import * as express from 'express';
import { Server } from 'http';
import socketio from 'socket.io';
import { IConfig } from '../model/IConfig';
import { ILogger } from '../model/ILogger';
import { HealthCheck } from './Healthcheck.Class';
import { KafkaProducer } from './KafkaProducer.Class';

export class Routing {
    private app: express.Application;
    private logger: ILogger;
    private jsonParser: any;
    private _isMiddlewareSet: boolean = false;
    private _isRoutingSet: boolean = false;
    private healthCheck: HealthCheck;
    private socket: socketio.Namespace;

    public get isMiddlewareSet(): boolean {
        return this._isMiddlewareSet;
    }

    public set isMiddlewareSet(val: boolean) {
        this._isMiddlewareSet = val;
    }

    public get isRoutingSet(): boolean {
        return this._isRoutingSet;
    }

    public set isRoutingSet(val: boolean) {
        this._isRoutingSet = val;
    }

    public setSocket(theSocket: socketio.Namespace) {
        this.socket = theSocket;
    }

    constructor(private configParams: IConfig, app: express.Application, logger: ILogger) {
        this.app = app;
        this.configParams = configParams;
        this.logger = logger;
    }

    public setMiddleWare() {
        this.jsonParser = bodyParser.json();
        this.isMiddlewareSet = true;
    }
    public async setRoutes(server: Server, producer: KafkaProducer) {
        this.app.get('/version', (req: any, res: any) => {
            if (this.configParams && this.configParams.version && this.configParams.version.length) {
                res.status(200).send(`<h1>${this.configParams.version}</h1>`);
            } else {
                res.sendStatus(500);
            }
        });
        this.app.get('/healthcheck', this.jsonParser, (req, res) => {
            this.healthCheck = new HealthCheck(this.configParams, this.logger, producer);
            this.healthCheck.pulse( (status: number, message: string) => {
                if (!message) {
                    res.sendStatus(status);
                } else {
                    res.status(status).send(`${message}\n`);
                }
            });
        });
        this.isRoutingSet = true;
    }
}
