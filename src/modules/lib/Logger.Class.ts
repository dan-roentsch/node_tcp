import Bunyan from 'bunyan';
import { IConfig } from '../model/IConfig';
import { ILogger } from '../model/ILogger';

export class Logger {
    private logger: ILogger;
    private app: string;
    private ConfigParams: IConfig;

    constructor(appName: string, config: IConfig) {
        this.app = appName;
        this.ConfigParams = config;
    }

    public getLogger() {
        if (this.logger) {
            return this.logger;
        }

        if (this.ConfigParams.log.stream == 'console') {
            this.logger = this.getConsoleLogger(this.ConfigParams.log.level);
        } else {
            this.logger = this.getFileLogger(this.ConfigParams.log.folder,
                this.ConfigParams.log.level);
        }

        return this.logger;
    }

    private getLogLevelFromString(level: string): Bunyan.LogLevel {
        if (level === 'trace') { return Bunyan.levelFromName.trace; }
        if (level === 'debug') { return Bunyan.levelFromName.debug; }
        if (level === 'info') { return Bunyan.levelFromName.info; }
        if (level === 'warn') { return Bunyan.levelFromName.warn; }
        if (level === 'error') { return Bunyan.levelFromName.error; }
        return 0;
    }

    private getConsoleLogger(level: string) {
        return Bunyan.createLogger({
            level: this.getLogLevelFromString(level),
            name: this.app,
            serializers: Bunyan.stdSerializers,
        });
    }

    private getFileLogger(path: string, level: string) {
        return Bunyan.createLogger({
            name: this.app,
            streams: [
                {
                    count: 5,
                    level: this.getLogLevelFromString(level),
                    path,
                    period: '1d',
                    type: 'rotating-file',
                }],
        });
    }
}
