import * as Kafka from 'kafka-node';
import * as os from 'os';
import { IConfig } from '../model/IConfig';
import { ILogger } from '../model/ILogger';

export class KafkaProducer {
  private logger: ILogger;
  private configParams: IConfig;
  private producer: Kafka.HighLevelProducer;
  private client: Kafka.Client;

  constructor(config: IConfig, theLogger: ILogger) {
    this.configParams = config;
    this.logger = theLogger;
    this.buildHighLevelProducer();
  }

  public closeClient() {
    this.client.close();
  }
  public sendPayload(topicName: string, message: string) {
    const payload: Kafka.ProduceRequest[] = [{
      messages: [message],
      topic: topicName,
    }];
    this.producer.send(payload, (err: any, data: Kafka.ProduceRequest[]) => {
      if (err) {
        this.logger.error({ message: 'Kafka producer error', error: err.message ? err.message : err });
      } else {
        this.logger.info(`1 message sent: ${JSON.stringify(payload)}`);
      }
    });
  }

  private buildHighLevelProducer() {
    this.client = new Kafka.Client(`${this.configParams.zookeeperHost}/`, 'notify-kafka-client');
    this.producer = new Kafka.HighLevelProducer(this.client);
    this.logger.info('Kafka producer starting');

    this.producer.on('error', (err: any) => {
      this.logger.info({ message: 'Kafka producer error', error: err.message ? err.message : err });
    });

    this.producer.on('ready', () => {
      this.logger.info('kafka producer ready');
    });
  }
}
