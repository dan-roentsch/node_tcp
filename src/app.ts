import express from 'express';
import { createServer, Server } from 'http';
import newrelic from 'newrelic';
import socketio from 'socket.io';
import { Circuit } from './modules/lib/Circuit.Class';
import { Configuration } from './modules/lib/Configuration.Class';
import { KafkaProducer } from './modules/lib/KafkaProducer.Class';
import { Logger } from './modules/lib/Logger.Class';
import { Routing } from './modules/lib/Routing.Class';
import { IConfig } from './modules/model/IConfig';
import { ILogger } from './modules/model/ILogger';

const APP = 'Notify';

export class Application {
    public app: express.Application;
    public ConfigParams: IConfig;
    public server: Server;
    public circuits: Circuit[];
    private logger: ILogger;
    private io: socketio.Server;
    private nsp: socketio.Namespace;
    private kafkaProducer: KafkaProducer;

    constructor() {
        this.app = express();
        this.server = createServer(this.app);
    }
    public async boot(optionalLogger?: ILogger): Promise<ILogger> {
        const Config = new Configuration();
        this.ConfigParams = await Config.getParams();
        if (!optionalLogger) {
            this.logger = new Logger(APP, this.ConfigParams).getLogger();
        } else {
            this.logger = optionalLogger;
        }
        return this.logger;
    }
    public buildServer(routing: Routing): void {
        try {
            this.kafkaProducer = new KafkaProducer(this.ConfigParams, this.logger);
            this.logger.info(`zookeeper address: ${this.ConfigParams.zookeeperHost}`);
            this.logger.info(`brokerhost address: ${this.ConfigParams.kafkaBrokerHost}`);
            this.logger.info(`New Relic application name: ${this.ConfigParams.newRelicAppName}`);
            this.buildModules();
            this.setRouting(routing);
            this.setServer();
        } catch (e) {
            this.logger.error(`Error creating server: ${e}`);
        }
    }

    public buildModules() {
        let room = new Circuit('/rooms',
                                'notify.rooms.all.incoming',
                                this.server,
                                this.logger,
                                this.ConfigParams,
                                true);

        let fpt = new Circuit('/fpt',
                                'notify.fpt.incoming',
                                this.server,
                                this.logger,
                                this.ConfigParams,
                                false);

        this.circuits = Array.of(room, fpt);
    }

    public setRouting(routing: Routing): void {
        this.logger.info(`Setting Middleware...`);
        routing.setMiddleWare();
        this.logger.info(`Setting Routes...`);
        routing.setRoutes(this.server, this.kafkaProducer);
    }
    public setServer(): void {
        this.server.listen(this.ConfigParams.serverPort, () => {
            this.logger.info(`Notify server listening on port ${this.ConfigParams.serverPort}`);
        });
    }
}

if (require.main === module) {
    const main = new Application();
    main.boot()
        .then((logger) => {
            logger.info(`In promise post boot()`);
            let routing = new Routing(main.ConfigParams, main.app, logger);
            main.buildServer(routing);
        })
        .catch((e) => {
            console.info(`Error creating server: ${e}`);
        });
}
