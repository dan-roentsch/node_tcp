module.exports = {
  apps : [{
    name: 'NOTIFY',
    script: 'app.js',
    instances: 1,
    autorestart: true,
    watch: false,
    exec_mode: "fork",
  }],
};
