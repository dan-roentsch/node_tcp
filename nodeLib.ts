// A data structure indicating which files/directories are copied from src/node_modules to dist/node_modules.
export class NodeLib {
    constructor() { }
    public deletables(): string[] {
        return [
            'node_modules/@types/',
        ]
    }
    public libs(): any[] {
        return [
            {
                dest: 'node_modules',
                src: 'node_modules/**/*.js',
            },
            {
                dest: 'node_modules',
                src: 'node_modules/**/*.json',
            },
        ];
    }
    public externals(): any[] {
        return ['@types'];
    }
}
