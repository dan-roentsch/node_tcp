FROM node:10.16.3-stretch
LABEL maintainer=dan_roentsch_lms_mhe

RUN \ 
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y wget && \
    apt-get install -y curl && \
    mkdir -p /home/notifyuser/.pm2 && \
    export PM2_HOME=/home/notifyuser/.pm2 && \
    npm install -g pm2 && \
    pm2 install pm2-logrotate && \
    pm2 set pm2-logrotate:max_size 1K  && \
    pm2 set pm2-logrotate:retain 2 && \
    pm2 set pm2-logrotate:compress false && \
    pm2 set pm2-logrotate:rotateInterval '*/10 * * * *' &&\
    useradd -m notifyuser | chpasswd && adduser notifyuser sudo

COPY dist/ /home/notify/
COPY scripts/docker-entrypoint.sh /home/notify/

RUN \
    chmod 777 /home/notify/docker-entrypoint.sh && \
    chown -R notifyuser home/*

EXPOSE 7020
EXPOSE 9092

USER notifyuser

ENTRYPOINT ["/home/notify/docker-entrypoint.sh"]
